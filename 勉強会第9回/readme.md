# 勉強会第9回



## 今回やること
numpy 入門


## 参考
http://rest-term.com/archives/2999/
http://www.kamishima.net/mlmpyja/nbayes1/ndarray.html

  
## numpy とは
機械学習やデータ分析では大規模なベクトル演算が必要となり，リストなどで演算すると時間がかかる
python ではforループなどが遅延の原因

⇒　numpy を使うことで高速なベクトル演算が可能

⇒　forループを numpyの演算に置き換えることで高速化

また，scipy，mathplotlib, pandasなどがnumpyに依存している

多くの機械学習手法では，numpyを使うのが当たり前となっている．

⇒　つまり，numpy 理解せずして機械学習は不可！






## numpy 配列とは
numpy 配列は多次元配列を扱うクラス.

基本的に以下の制約がある

```
配列内要素の型は全て同じ
配列長は固定 (固定長配列)
配列の各次元の要素数は同じ 
```

⇒ C言語などの配列と似ている


numpy配列の別名
* 配列
* numpy.ndarray (クラス名）
* array

補足

Q 行列という呼び名は使わないの？

A numpyにはmatrix(行列)というクラスが別に存在するが，文献によっては2次元配列を行列と呼ぶときもある






## numpy　の利用準備

* numpy のインポート

```python
import numpy as np
```


* numpyのインストール

インポートで「numpyがないよ！」と怒られたらインストールする


コマンドラインで
```
pip install numpy
```




## numpy 配列の宣言 ⇒ np_sample_1.py

numpy配列は基本的に

```python
np.array([array])
```

で宣言できる．

arrayにはpythonのリストやタプルを入れることが出来る．

なお，型の指定もできる

⇒ 引数に dtype = DATA_TYPE　を指定(指定しないとnp.int64)

DATA_TYPE　例

    np.int32
    
    np.float32
    
    np.float64
    
    

* 一次元配列の宣言

```python
np.array([1, 2, 3])
```

* 二次元配列の宣言

```python
np.array([[1, 2, 3], [4, 5, 6]])
```

* 特殊な配列の宣言
    
    * 要素が全てゼロの配列
    
    ```python
    np.zeros(5)
    np.zeros([3, 4])
    ```
  
    * 要素が全て１の配列
    
    ```python
    np.ones(5)
    np.ones([3,4])
    ```

    * 領域の確保(初期化無し）
    
    ```python
    np.empty(5)
    np.empty([3,4])
    ```
    
    * 配列のコピー
    
       * シャロ―コピー
    
        ```python
        arr_a = arr_b
        ```
    
        * ディープコピー
    
        ```python
        arr_b = arr_a.copy()
        ```
 
    * 乱数 np.random 
    
        * 0~1の範囲で連続一様分布に従う乱数
    
        ```python
        np.random.rand() #単一の値
        np.random.rand(3)　
        np.random.rand(2,5)
        ```
   
        * 標準正規分布 N(0, 1)に従う乱数

        ```python
        np.random.randn()
        np.random.randn(3)
        np.random.randn(2,5)
        ```
    
        * 標準正規分布 N(μ,σ^2)に従う乱数
    
        ```python
        sigma * np.random.randn() + mu
        ```
    
        * 決めた範囲の離散一様分布に従う乱数
        

        ```python
        np.random.randint(1, 10)
        ```
        
    * 単位行列　（正方行列）
  
    ```python
    np.identity(5)
    ```
    
    * 単位行列　
    
    eye(行, 列, 対角線位置)
    
    ```python
    np.eye(3, 2)
    np.eye(3, 4, 1) 
    ```
  
    * 連続値 
    
        * np.arange(始点，終点，増分）
    
        ```python
        np.arange(5)
        np.arange(1.0, 2.0, 0.1) 
        ```
    
        * np.linspace(始点，終点，要素数)
    
        ```python
        np.linspace(1, 10, 5)
        ```
    
        * np.logspace(始点べき乗，終点べき乗，要素数)
    
        ```python
        np.logspace(3, 5, 6) #常用対数
        np.logspace(3, 5, 6, base = 2) #底の指定
        ```
    
    * 繰り返し
    
    ```python
    np.tile([1, 2, 3], 3)
    ```
 
    * 格子配列
    
    ```python
    arr_a, arr_b = np.meshgrid( [1,2,3], [4,5,6,7] )
    ```
    
    * 三角行列
 
    ```python
    np.tri(5)
    ```

    * 対角線抜出
    
    ```python
    arr = np.array([[0,1,2], [3,4,5], [6,7,8]])
    np.diag(arr)
    ```
   

## numpy 配列の参照

リストと大体同じ
多次元の表現が違うので注意


```python
arr[0]
arr[0, 2]
```

最後の要素
```python
arr[-1]
```

スライス
```python
arr[0:3]
arr[1:3, 0:2]
```

行の抽出
```python
arr[0,:]
```

列の抽出
```python
arr[:, 2]
```


## numpy配列の計算

* 配列のスカラー倍

```python
arr * 3
```

* 配列の全要素をオフセット

```python
arr + 5
```

* 配列同士の計算 

同じ形の配列同士の場合，同じ位置同士で計算される

使える演算子

```
加算　a + b    # np.add(a,b)
減算　a - b    # np.subtract(a,b)
乗算  a * b    # np.multiply(a,b)
除算  b / a    # np.divide(b,a)
剰余  a % b    # np.mod(a,b)
冪乗  a ** b   # np.power(a,b)
符号反転  -a       # np.negative(a)
```

```python
arr_a * arr_b
arr_a + arr_b 
```


二次元配列と一次元配列の計算では、二次元配列の列(column)の数が一次元配列の長さと同じである時に次元配列の行一つ一つを一次元配列と計算させた結果が返る

```python
arr_v = np.array([2, 2, 3])
arr_a + arr_v
```

内積

```python
arr_a.dot(arr_b)
```

外積

```python
arr_a.cross(arr_b)
```



## 使えると便利な機能

* 変数の型

    * 現在の型取得
         
        arr.dtype    

    * 型を変える
       
        arr.astype(DATA_TYPE)


* 配列の大きさ


    * 要素数
        
        
        
        ```
        arr.size
        ```
    
    * 次元数の取得
        
        
        ```
        arr.ndim
        ```
    
    * 各次元ごとの配列の大きさ

        ```
        arr.shape
        ```

    * 配列のサイズ変更
    
    
        * 戻り値を返す
    
        ```    
        arr.reshape(shape)
        ```

        * 元の配列を変更
        
        
        ```
        arr.resize(shape)
        arr.shape = shape 
        ```
        
        例 
        
        ```
        arr.shape = 2,3
        
        arr.shape = 3, -1, 5 #-1 は自動設定
        ```
        
        * 一次元配列に変更
        
        ```
        arr.flatten()
        ```

## 今後

* numpy 関数


max

argmax

など

* 配列の結合，分割

* 配列のソート

* 配列の条件による置換

np.where

など


課題

リストと同じ課題をnumpyを使って実現する

課題が出来た人は課題提出場所フォルダの中に，新しく自分の名前を付けたフォルダを作成して課題ファイルを中に提出．















