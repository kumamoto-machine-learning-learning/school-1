# 勉強するときにおすすめのサイト

### 総合
* 松尾研究室が公開してるとてもいいページ
 * http://weblab.t.u-tokyo.ac.jp/deep-learning基礎講座演習コンテンツ-公開ページ/

### gitlab関連
* markdownの書き方
  * https://qiita.com/Qiita/items/c686397e4a0f4f11683d
  * https://ja.wikipedia.org/wiki/Markdown
  * そもそもmarkdownって何なのか https://qiita.com/Blueman81/items/72ca43681d16d44e21ad

* git (覚えたらプログラム直接取ってきたりアップしたりできるよ)
  * __試しでコマンド打ったりするときはブランチ切ってください__
  * gitとは https://qiita.com/nutsinshell/items/96cb83aecf9d09a7a8bc
  * おすすめソフト Sourcetree https://www.sourcetreeapp.com
  * gitlabとsourcetreeの連携 https://qiita.com/bachtk/items/8d92bcf38f45f7a73ef8
  * gitのコマンド https://qiita.com/konweb/items/621722f67fdd8f86a017

### Python 関連
* Python-izm https://www.python-izm.com
* Paiza(実際にコーディングしながら学べます) https://paiza.jp/works
* Keras サンプルプログラム https://github.com/keras-team/keras/tree/master/examples