# 勉強会 第二回

参考サイト
http://miyamo765.hatenadiary.jp/entry/2017/05/04

sonyのツール
https://dl.sony.com/ja/

■EXCELマクロの参考ソース
Option Explicit

Sub dataCreate()
     Dim wkst As String
     Dim r As Integer
     Dim obj
     Dim bottom As Integer
     Dim out As String
     Dim datFile As String
     Dim w As Object
     
     
     'ワークシート名は各自変更
     wkst = "Sheet1"
     
　　　
　　'CSVデータ開始位置


     bottom = Range("A2").End(xlDown).Row
     For r = 1 To bottom - 1
           out = Worksheets(wkst).Cells(r + 1, 1).Value & "," & Worksheets(wkst).Cells(r + 1, 2).Value　'パラメーターの数は増えたら追加要！
           datFile = ActiveWorkbook.Path & "\" & r & ".csv"
           
           Set obj = CreateObject("ADODB.Stream")
           
           obj.Charset = "UTF-8"
           obj.Open
           obj.WriteText (out)
           obj.Position = 0
           obj.Type = 1
           obj.Position = 3
        
         
          Dim byteData() As Byte '一時格納用
          byteData = obj.Read 'ストリームの内容を一時格納用変数に保存
          obj.Close '一旦ストリームを閉じる（リセット）
 
         obj.Open 'ストリームを開く
         obj.Write byteData 'ストリームに一時格納したデータを流し込む
         obj.SaveToFile datFile, 2
         obj.Close
       Next
End Sub
