# 勉強会第10回



## 今回やること
numpy 入門 その二


## 数学に必要な各種演算

* 四則演算
* 三角関数
* 指数，対数

など

https://deepage.net/features/numpy-math.html

## 統計に必要な関数

* sum
* max
* min

など

http://pppurple.hatenablog.com/entry/2016/05/02/235158
  
  
## 配列の連結

* 各種stack
* 各種split 

http://python-remrin.hatenadiary.jp/entry/concatenate
http://tanukigraph.hatenablog.com/entry/2017/08/23/195756


## ソート，転置，回転

* sort
* transpose

https://qiita.com/supersaiakujin/items/c580f2aae90818150b35

## 置換

* np.where

https://note.nkmk.me/python-numpy-where/


## 課題

## 課題１
  適当なRGB画像(3次元）を取り込み，グレースケール(1次元)に変換し表示する．
　

ヒント

画像の読み込み

from PIL import Image

import numpy as np

from matplotlib import pylab as plt

img = np.array( Image.open('foo.jpg') )


　
グレースケールへの変換


Y = 0.299×R + 0.587×G + 0.114×B

画像の表示

plt.imshow( img )



## 課題２
　課題１のグレースケール画像の輝度値を反転した画像を表示する
　
## 課題3 
  課題1のグレースケール画像を任意の閾値で2値化する
  
## 課題4
  課題１のグレースケール画像を横に2つ並べた画像を生成する．
  出来たら，縦横に2枚づつ，合計4枚並べる
 

　
　
　
　
