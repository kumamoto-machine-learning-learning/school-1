from PIL import Image

import numpy as np

from matplotlib import pylab as plt

img = np.array( Image.open( "index.png" ) )

# R, G, B = np.split( img, 3 , axis = 2)

R = img[:,:,0]

G = img[:,:,1]

B = img[:,:,2]

gray = 0.299*R + 0.587*G + 0.114*B


threshold = 100

bin_img = np.where( gray < threshold , 0, 255)

plt.imshow( bin_img, cmap = 'gray' )

plt.show()


