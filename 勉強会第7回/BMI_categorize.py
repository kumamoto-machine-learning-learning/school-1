# -*- coding: utf-8 -*-

import keras
import numpy as np
import pandas as pd

from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import *

#結果比較用にシード値を固定
np.random.seed(1337)

#データ取得用の関数
def GenData(flag = 0):
    #関数の引数にフラグを指定し、
    #学習時はflag == 0, テスト時はflag == 1
    #としてそれに対応したデータセットを取得
    if ( flag == 0):
        dataFile = "tra.csv"
    elif ( flag == 1 ):
        dataFile = "val.csv"

    #指定されたファイルから呼び出し、行列に格納する
    buf = pd.read_csv(dataFile).as_matrix()

    X = buf[ : , 0 : 2]  #行列から0行~3行目までを取り出す
    Y = buf[ : , 2:3] #行列から4行目を取り出す

    return [ X, Y]

#トレーニング用データを取得
X_train, Y_train = GenData(0)
#テスト用データを取得
X_test, Y_test = GenData(1)

num_classes = 3
Y_train = keras.utils.to_categorical( Y_train, num_classes)
Y_test = keras.utils.to_categorical( Y_test, num_classes)

#モデルの構築
model = Sequential()
model.add(Dense( 4, input_shape = ( 2,), activation = 'linear'))
model.add(Dense( num_classes, activation = 'softmax'))
model.compile(loss = 'categorical_crossentropy', optimizer = 'adam', metrics=['acc'])

#パラーメータ バッチサイズ  = 32 , 繰り返し回数 = 200
BATCH_SIZE = 32
ITERATION = 100
#学習開始
history = model.fit( X_train, Y_train, batch_size = BATCH_SIZE, epochs = ITERATION, verbose = 1, validation_data = (X_test, Y_test))

# 学習済みモデルを、テストデータを用いて評価
score = model.evaluate( X_test, Y_test, verbose = 1)
res = model.predict(X_test, batch_size = 1, verbose = 1)
print('Test score : ',score)

#実際に学習済みモデルに任意の値を適用して試してみる
hight = float(input('身長を入力してください > ')) / 1000.0
weight = float(input('体重を入力してください > ')) / 100.0

user = np.array([[weight, hight]])
res = model.predict( user, batch_size = 1, verbose = 0)

print(res)
#判別の閾値
threshold = 0.75

if ( res[0][1] > threshold):
    body = "痩せ型"
elif( res[0][0] > threshold):
    body = "普通"
elif( res[0][2] > threshold):
    body = "太り気味"
else:
    body = "判別不能(学習不足)"
print('あなたのタイプは' + body + 'です(学習)')

#実際のタイプ
fact = weight * 100 / (hight * hight * 100)
if (fact < 18.5):
    print("あなたのタイプは痩せ型です(理論)")
elif(fact > 25):
    print("あなたのタイプは太り気味です(理論)")
else:
    print("あなたのタイプは普通です(理論)")
