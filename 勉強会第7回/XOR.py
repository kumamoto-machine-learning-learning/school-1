# -*- coding: utf-8 -*-
import numpy as np

#from keras.models import Sequential, model_from_json
from keras import models
#from keras.layers.core import Dense
from keras.layers import core
#from keras.optimizers import RMSprop
from keras import optimizers

# Input data
X_train = np.array([[0.0, 0.0],
                    [1.0, 0.0],
                    [0.0, 1.0],
                    [1.0, 1.0]])
# Teacher for input
Y_train = np.array([0.0,
                    1.0,
                    1.0,
                    0.0])

# Build model
model = models.Sequential()
output_count_layer0 = 4
model.add(core.Dense(output_count_layer0, input_shape=(2,), use_bias=True, activation='sigmoid')) # Need to specify input shape for input layer
output_count_layer1 = 1
model.add(core.Dense(output_count_layer1, use_bias=True, activation='linear'))
model.compile(loss='mean_squared_error', optimizer=optimizers.RMSprop(), metrics=['accuracy'])

# Start training
BATCH_SIZE = 4
ITERATION = 3000
history = model.fit(X_train, Y_train, batch_size=BATCH_SIZE, epochs=ITERATION, verbose=1)

# Evaluate model
X_test = X_train
Y_test = Y_train
score = model.evaluate(X_test, Y_test, verbose=1)
print('Test score:', score[0])
print('Test accuracy:', score[1])
