# 勉強会第7回

### 次回すること
Pythonのリスト・配列・行列・NumPyあたりの勉強  
BMIのプログラム改良して誰が一番良くなるかコンテスト  
輪行の担当決め  

## VisualStudio Codeをセットアップしていない人は事前にセットアップしてください。
https://qiita.com/Atupon0302/items/ee3303629ce0b2ae58d7

##サンプルプログラム
http://27.gigafile.nu/0208-b9d3f3f267a46265a530a33474db7542f
（前回落とした人は不要です。）
## ele_prediction.py 説明

### 1~9行目
実行に必要なライブラリの読み込み
  * keras
    * ディープラーニングのフレームワーク
    * バックエンドでtensorflowを呼び出している
  * numpy
    * pythonの数値計算用ライブラリ
  * pandas
    * データ解析、操作用ライブラリ  

```python
import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
np.random.seed(1337)
from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import *
```



### 12~27行目 GenData( flag = 0 )
学習データ、テストデータ呼び出し用の関数
* 引数としてflagを渡している
  * flag == 0 -> 学習用データの呼び出し
  * flag == 1 -> テストデータの呼び出し
* 22行目: 指定したファイルから呼び出し、行列としてbufに格納
* 24行目: buf[ : , 0 : 4]
  * []内の ' , ' より前は取り出す行の指定で、この場合は全行選択
  * []内の ' , ' より後ろは取り出す列の指定で、[ a : b ]の場合、a列目からb列目の前、つまりb-1列目まで選択
* 25行目: 24行目と同様
* 27行目: この関数を実行した際の返り値を指定。

```python
def GenData(flag = 0):
    if ( flag == 0):
        dataFile = "train.csv"
    elif ( flag == 1 ):
        dataFile = "test.csv"

    buf = pd.read_csv(dataFile).as_matrix()
    X = buf[ : , 0 : 4]
    Y = buf[ : , 4:5]

    return [ X, Y]
```

### 29~32行目
データの用意
先程定義したGenData()関数を用いて教師データ、テストデータをそれぞれ取得

```python
#トレーニング用データを取得
X_train, Y_train = GenData(0)
#テスト用データを取得
X_test, Y_test = GenData(1)
```

### 34~38行目
ネットワークモデルの定義
* Sequential() : シーケンシャルモデルの定義
  * https://keras.io/ja/models/sequential/
  * 定義したモデルにaddすることで層を追加する  
* 初段のみinput_shape(入力されるデータのサイズ)の指定が必要だが、2段目以降は前段の出力に応じて変更されるため指定する必要なし。
* Dense : 全結合層
  * https://keras.io/ja/layers/core/#dense
* compile : モデルのコンパイル
 * loss : 損失関数
 * optimizer : 最適化手法
 * metrics : 評価関数(分類問題などで用いるが今回は回帰問題のため使用していない)  

```python
 #モデルの構築
 model = Sequential()
 model.add(Dense( 8, input_shape = ( 4,), use_bias = True, activation = 'tanh'))
 model.add(Dense( 1, use_bias = True, activation = 'relu'))
 model.compile(loss = 'mean_squared_error', optimizer = 'adam')
 ```

### 40~44行目
学習過程
* BATCH_SIZE : バッチサイズの指定
* ITERATION : 繰り返し回数
* history : 学習過程を入れ込む
* model.fit : 学習を行う。
  * 引数
    * 1番目 : 入力データ
    * 2番目 : 目標とする出力データ
    * batch_size : バッチサイズ(同時に学習を行うデータセットのサイズ)
    * epochs : 学習回数
    * verbose : 結果出力の有無  

  ```python
  BATCH_SIZE = 16
  ITERATION = 1000
  #学習開始
  history = model.fit( X_train, Y_train, batch_size = BATCH_SIZE, epochs = ITERATION, verbose = 1, validation_data = (X_test, Y_test))
  ```
### 46~50行目
先程学習したモデルをテストデータを用いて評価する
* evaluate : モデルの評価
* predict : モデルを用いて、予測取得する  

```python
score = model.evaluate( X_test, Y_test, verbose = 1)
res = model.predict(X_test, batch_size = 1, verbose = 1)
print(res)
print('Test score : ',score)
```

### 52,53行目
結果表示画像用の変数定義  
```python
fig, axL = plt.subplots(ncols=1, figsize=(10,4))
fig2, temp = plt.subplots(ncols = 1, figsize = (10,10))
```

### 55~70行目
グラフ表示する内容を定義する関数
* plot_result : 2変数を同時にグラフに出力し、結果の比較を可能にする
* plot_history_loss : 学習過程の誤差の変化を出力  

```python
def plot_result( res, ans):
    temp.set_title('diff between res and ans')
    temp.plot(res, label = 'predicted result')
    temp.plot(ans, label = 'correct result')
    temp.legend(loc = 'upper right')
    temp.set_ylim([0,1])

# loss
def plot_history_loss(fit):
    axL.plot(fit.history['loss'],label="loss for training")
    axL.plot(fit.history['val_loss'],label="loss for validation")
    axL.set_title('model loss')
    axL.set_xlabel('epoch')
    axL.set_ylabel('loss')
    axL.legend(loc='upper right')
```

### 72~76行目
グラフの出力
* 72,73行目 : グラフ表示用関数の定義
* 74,75行目 : グラフの保存  

```python
plot_history_loss(history)
plot_result( res, Y_test)
fig.savefig('result.png')
fig2.savefig('result2.png')
plt.close()
```

## mnist.py 説明
このプログラムでしてること MNISTのデータセットを使って、10クラスに画像の分類を行っている

### 7~13行目
実行に必要なライブラリの読み込み
* \__future__
  * python2.* でpython3.* 風の記述ができるようにできる(らしい)
* keras
  * ディープラーニングのフレームワーク

### 15~17行目
学習パラメータの設定

### 20行目
データの取得
* kerasでは標準でmnistのデータセットをサポートしており、kras.datasets.mnist.load_data()で教師データ、テストデータの取得が可能
* x_train, y_train, x_test, y_testにデータを格納

### 22~29行目
データ配列のリシェイプ、リシェイプ後の大きさの表示
* 22行目: x_trainをリシェイプ
  * 現在のデータ構造がデータ点数*縦*横 = 60000 * 32 * 32
  * リシェイプ後のデータ構造は 60000 * 784 で、データの総数は変わらないが構造が変化している。
* 23行目: 22行目と同様
* 24,25行目: 変数の型を指定。このネットワークではfloat32で計算を行う
  * GPUで演算をする際に、HPC向けではない一般向けのGPUは倍精度(=double)より単精度(=float32)の方が速度が圧倒的に出るため、若干の桁落ちよりも速度を優先しfloat32での処理を行っている
* 26,27行目: 取得した画像のそれぞれの画素の値を0~1にリサイズ

### 32,33行目
画像ラベルからカテゴリー配列への変換
* kerasのto_categorical関数を用いて、画像ラベルからカテゴリー配列へと変換している。  
例 クラス数10の場合 :   
0 -> [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]  
1 -> [ 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]  
…  
9 -> [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]  

### 35~46行目
ネットワークモデルの定義
* Dropout : ドロップアウト層
  * 0 ~ 1の間で定義
  * 定義された割合だけ、前の層のユニットを無効化する
  * 過学習防止の為用いられる  [参照](http://sonickun.hatenablog.com/entry/2016/07/18/191656)
* summary() : ネットワークモデルの要約を出力    
例:   mnist.py

```python
_________________________________________________________________
Layer (type)                 Output Shape              Param #
=================================================================
dense_1 (Dense)              (None, 512)               401920
_________________________________________________________________
dropout_1 (Dropout)          (None, 512)               0
_________________________________________________________________
dense_2 (Dense)              (None, 512)               262656
_________________________________________________________________
dropout_2 (Dropout)          (None, 512)               0
_________________________________________________________________
dense_3 (Dense)              (None, 10)                5130
=================================================================
Total params: 669,706
Trainable params: 669,706
Non-trainable params: 0
_________________________________________________________________
```

### 48~52行目
モデルのコンパイル
* validation_data : バリデーション(モデルの妥当性の確認)用のデータセット指定
  * バリデーション用のデータは評価のみで学習には用いられないため、テストデータと同様のものを用いて良い

### 53~55行目
学習済みモデルの評価、評価結果出力
