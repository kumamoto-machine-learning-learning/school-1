import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#結果比較用にランダムシードを固定
np.random.seed(1337)
from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import *

#データ取得用の関数
def GenData(flag = 0):
    #関数の引数にフラグを指定し、
    #学習時はflag == 0, テスト時はflag == 1
    #としてそれに対応したデータセットを取得
    if ( flag == 0):
        dataFile = "train.csv"
    elif ( flag == 1 ):
        dataFile = "test.csv"

    #指定されたファイルから呼び出し、行列に格納する
    buf = pd.read_csv(dataFile).as_matrix()

    X = buf[ : , 0 : 4]  #行列から0行~3行目までを取り出す
    Y = buf[ : , 4:5] #行列から4行目を取り出す

    return [ X, Y]

#トレーニング用データを取得
X_train, Y_train = GenData(0)
#テスト用データを取得
X_test, Y_test = GenData(1)

#モデルの構築
model = Sequential()
model.add(Dense( 8, input_shape = ( 4,), use_bias = True, activation = 'tanh'))
model.add(Dense( 1, use_bias = True, activation = 'relu'))
model.compile(loss = 'mean_squared_error', optimizer = 'adam')

#パラーメータ バッチサイズ  = 32 , 繰り返し回数 = 200
BATCH_SIZE = 16
ITERATION = 1000
#学習開始
history = model.fit( X_train, Y_train, batch_size = BATCH_SIZE, epochs = ITERATION, verbose = 1, validation_data = (X_test, Y_test))

# 学習済みモデルを、テストデータを用いて評価
score = model.evaluate( X_test, Y_test, verbose = 1)
res = model.predict(X_test, batch_size = 1, verbose = 1)
print(res)
print('Test score : ',score)

fig, axL = plt.subplots(ncols=1, figsize=(10,4))
fig2, temp = plt.subplots(ncols = 1, figsize = (10,10))

def plot_result( res, ans):
    temp.set_title('diff between res and ans')
    temp.plot(res, label = 'predicted result')
    temp.plot(ans, label = 'correct result')
    temp.legend(loc = 'upper right')
    temp.set_ylim([0,1])

# loss
def plot_history_loss(fit):
    # Plot the loss in the history
    axL.plot(fit.history['loss'],label="loss for training")
    axL.plot(fit.history['val_loss'],label="loss for validation")
    axL.set_title('model loss')
    axL.set_xlabel('epoch')
    axL.set_ylabel('loss')
    axL.legend(loc='upper right')

plot_history_loss(history)
plot_result( res, Y_test)
fig.savefig('result.png')
fig2.savefig('result2.png')
plt.close()
