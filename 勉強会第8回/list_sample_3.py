list1 = [100, 200, 300, 400, 500]

list2 = ["知能", "太郎", "IQ", 200, "AGE", 21]

list3 = [["知能", "太郎"], ["IQ", 200], [ "AGE", 21, 2018, 2, 22], 100, 200]

print("list1 : "  + str(list1) )
print("list2 : "  + str(list2) )
print("list3 : "  + str(list3) )



print()
print("list1[1] -> 100, list1[2] -> 100, list1[1] -> 100")
list1[1] = 100
list1[2] = 100
list1[3] = 100
print("list1 : "  + str(list1) )


print()
print("list1[2:4] -> list3[0]")
print("list3[0] : "  + str(list3[0]) )
list1[2:4] = list3[0]
print("list1 : "  + str(list1) )


print()
print("list1[2:4] -> list3[2]")
print("list3[2] : "  + str(list3[2]) )
list1[2:4] = list3[2]
print("list1 : "  + str(list1) )



