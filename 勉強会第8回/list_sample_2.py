list1 = [100, 200, 300, 400, 500]

list2 = ["知能", "太郎", "IQ", 200, "AGE", 21]

list3 = [["知能", "太郎"], ["IQ", 200], [ "AGE", 21, 2018, 2, 22], 100, 200]

print("list1 : "  + str(list1) )
print("list2 : "  + str(list2) )
print("list3 : "  + str(list3) )


print()
print ("list1[0] : " + str(list1[0]) )
print ("list1[2] : " + str(list1[2]) )


print()
print ("list1[-1] : " + str(list1[-1]) )
print ("list1[-2] : " + str(list1[-2]) )

print()
print ("list3[0][1] : " + str(list3[0][1]) )
print ("list3[2][3] : " + str(list3[2][3]) )

print()
print ("list1[2:3] : " + str(list1[2:3]) )
print ("list1[1:-1] : " + str(list1[1:-1]) )

