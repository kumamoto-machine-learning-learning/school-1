# 勉強会第8回

## 今回やること
  * リスト 入門

## リスト　入門
参考
　https://www.pythonweb.jp/tutorial/list/
　

### リストの宣言方法 ⇒ list_sample_1.py
```
[obj1 , obj2, ...]
```


括弧[]で囲んだなかにオブジェクトをカンマ区切りで記述


```python
list1 = [100, 200, 300, 400, 500]
```
 
 
 
   
リストの要素は異なる型のオブジェクトでも可
```python
list2 = ["知能", "太郎", "IQ", 200, "AGE", 21]
```
 
   
   
リストの中にリストを宣言することもできる．

```python
list3 = [["知能", "太郎"], ["IQ", 200], [ "AGE", 21, 2018, 2, 22], 100, 200]
```

空のリストも作れる
```python
list4 = []
```


### リストの参照方法　⇒ list_sample_2.py

要素には先頭の要素から0から順にインデックスが割り当てられる
 
```python
list1[0]
list1[2]
```
   
   
インデックスに負の値を指定した場合にはリストの最後から要素を参照
```python
list1[-1]
list1[-2]
```
   
 
   
リストの中にリストがある場合

```python
list3[0][1]
list3[2][3]
```



リストの一部を取り出す(スライス）

```
list_obj[開始インデックス : 終了インデックス]
```


```python
list1[2:3]
list1[1:-1]
```

### リストの要素の変更 ⇒ list_sample_3.py

リストの要素を指定して値を代入することが出来る

```python
list1[1] = 100
list1[2] = 100
```

スライスを利用してリストを別のリストで置き換えることが出来る．

```python
list1[2:4] = list3[0]
```

置き換えの大きさが異なっても可

  ```python
  list1[2:4] = list3[2]
  ```

### リストの要素の追加，連結，挿入，削除　⇒ list_sample_4py

* 末尾に追加

  ```python
  list_obj.append(obj)
  ```

  リストの最後にオブジェクトを追加する

  ```python
  list1.append(600)
  list1.append([700,700])
  ```


* 末尾に複数の要素の追加

  ```python
  list_obj.extend(list_obj)
  ```
  
  複数の要素をリストの要素に追加する

  ```python
  list1.extend([800 , 900, 1000])
  ```
  
* リストの連結
  ```python
  list_obj + list_obj
  ```

extend()と同じ感じだが，元のリストに変更がない


  ```python
  list4 = list1 + [700, 800]
  ```


* 繰り返し連結
  
  ```python
  list_obj * 繰り返し数
  ```

  繰り返し数だけリストを連続して連結する

  ```python
  print (list1 * 3)
  ```

*　挿入

  ```python
  list_obj.insert(index, obj)
  ```

  リストの第1引数で指定した要素の前に要素を追加する

  ```python
  list4 = list1.insert(3, 450)
  ```

*　削除

  ```python
  del list_obj
  ```
  
  リストの指定した要素を削除する

  ```python
  del list1[3]
  del list1[1:3]
  ```
  
* 要素を取り出す( pop)

  ```python
  list_obj.pop(index)
  ```
  
  リストのインデックスで指定した要素を取り出す(pop)
  取り出された要素はリストから削除される．
  インデックスの指定がない場合末尾の要素を取り出す．

  ```python　
  num = list1.pop(4)
  print( num )
  ```
  
* 指定の値の要素を削除する

  ```python
  list_obj.remove(obj)
  ```

  リストの要素で指定したオブジェクトと同じものを全て削除する．

  ```python
  list1.remove(100)
  ```
  
  
### よく使う関数


* リストの長さを取得
　

  len(list_obj)


* 存在を調べる

　obj in list_obj

　
　objがリストに含まれるときはTrue そうでないとFalse
　
  obj not in list_obj
  　

* 指定の値を持つインデックス取得


  list_obj.index(obj)

　最初の要素のインデックスが返ってくる
　

* 指定の値の要素の数を数える

  list_obj.count(obj)


* 連続した要素のリストを作る

  range(start, stop, step)
  
  例
    range(3)
    ⇒　[0, 1, 2]


    range(2, 5)
    ⇒　[2, 3, 4]


    range(3, 10, 2)
    ⇒　[3, 5, 7, 9]

### 練習問題

* 問題１

  
　キーボードから５個の入力を受け取り，逆順で画面に表示するプログラムを作ろう
　
　
　
  ```python
  出力例
  ```
　
　　
* 問題2
  

　5つの数字を要素として持つリストを宣言する．
　キーボードから数字を入力し，リストのすべての要素を入力倍したリストを画面に表示する
　
  ```python
  
  出力例
  
  ```
  
　
　
* 問題3
  

　九九の表をリストで作ろう(手打ち禁止！　ループで)
  
  ```python
  
  出力例
  
  ```
  
　
* 問題3　(additional problem)
  

　問題２の九九の表を使って二つの入力の積をもとめる (乗算演算子を使わない)
  
  ```python
  
  出力例
  
  ```
  
課題が出来た人は課題提出場所フォルダの中に，新しく自分の名前を付けたフォルダを作成して課題ファイルを中に提出．

