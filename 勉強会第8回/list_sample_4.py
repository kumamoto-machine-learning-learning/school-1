list1 = [100, 200, 300, 400, 500]

list2 = ["知能", "太郎", "IQ", 200, "AGE", 21]

list3 = [["知能", "太郎"], ["IQ", 200], [ "AGE", 21, 2018, 2, 22], 100, 200]

print("list1 : "  + str(list1) )
print("list2 : "  + str(list2) )
print("list3 : "  + str(list3) )

# append
print()
list1.append(600)
print("list1.append(600)")
print("list1 : "  + str(list1) )

print("list1.append([700,700])")
list1.append([700,700])
print("list1 : "  + str(list1) )


# extend

print()
print("list1.extend([800 , 900, 1000])")
list1.extend([800 , 900, 1000])
print("list1 : "  + str(list1) )
  
# +
print()
print("list4 = list1 + [1100, 1200] + [1300]")
list4 = list1 + [1100, 1200] + [1300]

print("list1 : "  + str(list1) )
print("list4 : "  + str(list4) )


# * 
print()
print("list5 = [1, 2, 3] * 3")
list5 = [1, 2, 3] * 3
print("list5 : "  + str(list5) )
  


# insert
print()
print("list1.insert(6, 700)")
list1.insert(6, 700)
print("list1 : "  + str(list1) )

print("list1.insert(3, [400, 400])")
list1.insert(3, [400, 400])
print("list1 : "  + str(list1) )



# deleate
print()
print("del list1[3]") 
del list1[3]
print("list1 : "  + str(list1) )

print("del list1[7:-1]") 
del list1[7:-1]
print("list1 : "  + str(list1) )



#pop
print()
print("num = list1.pop(7)")
num = list1.pop(7)
print("num  :"+ str( num ) )
print("list1 : "  + str(list1) )



#remove
print()
list6 = [0, 0,  0, 1 , 0,  0, 2, 0, 0, 0, 0, 0, 0]
print("list6 : "  + str(list6) )
print("list6.remove(1)")
list6.remove(1)
print("list6 : "  + str(list6) )
print("list6.remove(2)")
list6.remove(2)
print("list6 : "  + str(list6) )
