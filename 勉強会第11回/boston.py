# coding: utf-8

# # Bostonデータセット 取扱 及び データの特徴

# In[5]:

# get_ipython().magic('matplotlib inline')

import numpy as np
import pandas as pd
from sklearn import datasets
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split

boston = datasets.load_boston()

data = np.c_[boston.data, boston.target]
col = np.append(boston.feature_names, 'MEDV')
df = pd.DataFrame(data, columns = col)
df.head()


# In[2]:

# データセットの説明
print(boston.DESCR)


# ### データセットの説明(詳細は上を)
# * CRIM -- 犯罪発生率(人口単位)
# * ZN -- 25,000平方フィート以上の住宅区画の割合
# * INDUS -- 非小売業の土地面積の割合(人口単位)
# * CHAS -- チャールズ川沿いか(1:Yes, 0:No)
# * NOX -- 窒素酸化物の濃度(pphm単位)
# * RM -- １戸あたりの平均部屋数
# * AGE -- 1940年よりも前に建てられた家屋の割合
# * DIS -- ボストンの主な5つの雇用圏までの重み付きの距離
# * RAD -- 幹線道路へのアクセス指数
# * TAX -- 10,000ドルあたりの所得税率
# * PTRATIO -- 教師あたりの生徒の数(人口単位)
# * B -- アフリカ系アメリカ人居住者の割合(人口単位)
# * LSTAT -- 低所得者の割合
# * MEDV(=Price) -- 住宅価格の中央値(単位 1,000ドル)(予測対象)

# ## 各値の相関関係

# In[3]:

plt.figure(figsize=(15,15))
corr = df.corr()
sns.heatmap(corr, annot=True)


# In[4]:

sns.pairplot(df)


# ### 教師データとテストデータのsplit

# In[10]:

X_train, X_test, y_train, y_test = train_test_split(df.drop("MEDV", 1), df.MEDV)


# ### 以下、学習
